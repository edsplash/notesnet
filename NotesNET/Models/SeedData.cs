﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using NotesNET.Data;

namespace NotesNET.Models
{
    public static class SeedData
    {
        private static DateTime NowWithoutSeconds()
        {
            var now = DateTime.Now;
            return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);
        }

        public static void Initialize(TodoDbContext context)
        {

            // Look for any existing entries.
            if (context.Items.Any())
            {
                return; // DB has been seeded
            }

            context.Items.AddRange(
                new TodoItem()
                {
                    CreatedDate = NowWithoutSeconds().AddHours(-26.75),
                    FinishDate = NowWithoutSeconds() + TimeSpan.FromDays(5),
                    Finished = false,
                    Importance = 5,
                    Title = "Besuch Kühlschranktechniker",
                    Text = "Baseball Schläger bereit halten."
                },
                new TodoItem()
                {
                    CreatedDate = NowWithoutSeconds().AddHours(-5.25),
                    FinishDate = NowWithoutSeconds() + TimeSpan.FromDays(1),
                    Finished = false,
                    Importance = 2,
                    Title = "Karton rausbringen",
                    Text = "Nicht am Vorabend bei Regen."
                },
                new TodoItem()
                {
                    CreatedDate = NowWithoutSeconds(),
                    FinishDate = NowWithoutSeconds() + TimeSpan.FromDays(1),
                    Finished = true,
                    Importance = 2,
                    Title = "Zeitungen bündeln",
                    Text = "Nicht am Vorabend bei Regen. Sonst um 7:00 Uhr."
                },
                new TodoItem()
                {
                    CreatedDate = NowWithoutSeconds().AddYears(-14).AddDays(3).AddHours(3),
                    FinishDate = NowWithoutSeconds() + TimeSpan.FromDays(4),
                    Finished = false,
                    Importance = 1,
                    Title = "Wäsche machen",
                    Text = "Weichspüler und Calgon-Tabs verwenden."
                },
                new TodoItem()
                {
                    CreatedDate = NowWithoutSeconds().AddMilliseconds(1),
                    FinishDate = NowWithoutSeconds() + TimeSpan.FromDays(3),
                    Finished = true,
                    Importance = 4,
                    Title = "Brot kaufen",
                    Text = "Nur Urdinkel Brot wenn es kein Weissbrot hat."
                }
            );
            context.SaveChanges();
        }
    }
}