﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Microsoft.Extensions.Localization;

namespace NotesNET.Models
{
    public class TodoItem
    {
        public long Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Titel muss angegeben sein")]
        [DisplayName("Titel")]
        public string Title { get; set; }

        [DisplayName("Details")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Wichtigkeit muss angegeben werden")]
        [DisplayName("Wichtigkeit")]
        [Range(1,5)]
        public int Importance { get; set; } = 0;

        [DisplayName("Erledigungstermin")]
        [DisplayFormat(NullDisplayText = "ohne Termin", ApplyFormatInEditMode = false, DataFormatString = "{0:d}")]
        public DateTime? FinishDate { get; set; }

        [DisplayName("Erstelldatum")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedDate { get; set; } = DateTime.Now;

        [DisplayName("erledigt")]
        public bool Finished { get; set; }
    }
}