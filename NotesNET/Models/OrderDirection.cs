﻿namespace NotesNET.Models
{
    public enum OrderDirection
    {
        Ascending,
        Descending
    }
}