﻿namespace NotesNET.Models
{
    public enum OrderType
    {
        FinishedDate,
        CreatedDate,
        Importance,
    }
}
