﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Logging;
using NotesNET.Data;
using NotesNET.Models;
using NotesNET.Services;

namespace NotesNET.Controllers
{
    public class TodoController : Controller
    {
        private readonly ITodoItemService _service;
        private readonly IViewSettings _viewSettings;

        public TodoController(ITodoItemService service, IViewSettings viewSettings)
        {
            _service = service;
            _viewSettings = viewSettings;
        }


        public IActionResult List()
        {
            var direction = _viewSettings.CurrentOrderDirection;

            var items = _service.SortOrder(_viewSettings.CurrentSortOrder, direction);
            items = _service.FilterFinished(_viewSettings.FilterFinishedItems, items);
            return View("List", items);
        }

        public IActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public IActionResult Create(TodoItem item)
        {
            if (ModelState.IsValid)
            {
                _service.AddItem(item);
                return RedirectToAction("List");
            }
            else
            {
                return View("Create", item);
            }            
        }

        public IActionResult Edit(long id)
        {
            var item = _service.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return View("Edit", item);
        }

        [HttpPost]
        public IActionResult Edit(TodoItem item)
        {
            if (ModelState.IsValid)
            {
                _service.UpdateItem(item);
                return RedirectToAction("List");
            }
            else
            {
                return RedirectToAction("Edit", new {id = item.Id});
            }
        }

        public IActionResult ChangeStyle(string newStyle)
        {
            _viewSettings.CurrentStyle = newStyle;
            return RedirectToAction("List");
        }

        public IActionResult ChangeSortOrder(OrderType newSortOrder, OrderDirection newOrderDirection)
        {
            //Set SortDirection to default when SortOrder changes
            if (_viewSettings.CurrentSortOrder != newSortOrder)
            {
                newOrderDirection = SessionBasedViewSettings.DefaultSortDirection;
            }

            _viewSettings.CurrentSortOrder = newSortOrder;
            _viewSettings.CurrentOrderDirection = newOrderDirection;
            return RedirectToAction("List");
        }

        public IActionResult ChangeFilterFinished(bool filterFinished)
        {
            _viewSettings.FilterFinishedItems = filterFinished;
            return RedirectToAction("List");
        }
    }
}