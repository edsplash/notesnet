﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NotesNET.Models;

namespace NotesNET.Controllers
{
    public class SessionBasedViewSettings : IViewSettings
    {
        private static readonly string styleKey = "style";
        private static readonly string filterFinishedKey = "filterFinished";
        private static readonly string sortOrderKey = "sortOrder";
        private static readonly string sortDirectionKey = "sortDirection";

        public static readonly string DefaultStyle = "light";
        public static readonly OrderType DefaultSortOrder = OrderType.FinishedDate;
        public static readonly bool DefaultFilterFinishedItems = false;
        public static readonly OrderDirection DefaultSortDirection = OrderDirection.Descending;

        private readonly IHttpContextAccessor _contextAccessor;
        

        public SessionBasedViewSettings(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        private ISession Session => _contextAccessor.HttpContext.Session;

        /// <summary>
        /// Used in the view to toggle between light and dark theme
        /// </summary>
        public string OppositeStyleOfCurrent => GetOppositeStyle(CurrentStyle);

        public string CurrentStyle
        {
            get => Session.GetString(styleKey) ?? DefaultStyle;
            set => Session.SetString(styleKey, value);
        }

        public bool FilterFinishedItems
        {
            get => GetBool(Session.GetInt32(filterFinishedKey)) ?? DefaultFilterFinishedItems;
            set => Session.SetInt32(filterFinishedKey, GetInt(value));   
        }

        /// <summary>
        /// Used in the view to toggle between ascending and descending
        /// </summary>
        public OrderDirection OppositeOfCurrentOrderDirection => CurrentOrderDirection == OrderDirection.Ascending ? OrderDirection.Descending : OrderDirection.Ascending;

        public OrderDirection CurrentOrderDirection
        {
            get
            {
                var directionValue = Session.GetInt32(sortDirectionKey);

                if (directionValue is null)
                {
                    return DefaultSortDirection;
                }
                else
                {
                    return (OrderDirection) directionValue;
                }
            }
            set => Session.SetInt32(sortDirectionKey, (int)value);
            
        }

        public OrderType CurrentSortOrder
        {
            get
            {
                var orderTypeValue = Session.GetInt32(sortOrderKey);

                if (orderTypeValue is null)
                {
                    return DefaultSortOrder;
                }
                else
                {
                    return (OrderType) orderTypeValue;
                }
            }
            set => Session.SetInt32(sortOrderKey, (int) value);
        }


        private static string GetOppositeStyle(string style)
        {
            switch (style)
            {
                case "dark": return "light";
                case "light": return "dark";
                default: return "light";
            }
        }

        private bool? GetBool(int? value)
        {
            if (value == null)
                return null;

            var intValue = value;

            return intValue == 1;
        }

        private int GetInt(bool value)
        {
            return value ? 1 : 0;
        }
    }
}
