﻿using NotesNET.Models;

namespace NotesNET.Controllers
{
    public interface IViewSettings
    {
        string OppositeStyleOfCurrent { get; }
        string CurrentStyle { get; set; }
        bool FilterFinishedItems { get; set; }
        OrderType CurrentSortOrder { get; set; }
        OrderDirection CurrentOrderDirection { get; set; }
        OrderDirection OppositeOfCurrentOrderDirection { get; }
    }
}