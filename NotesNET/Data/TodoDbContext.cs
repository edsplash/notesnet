﻿// ================================================================================================
// <copyright file="ApplicationDbContext.cs" company="Bruker Biospin AG">
//   ______ _  _   ____  _     
//  |  ____| || | |  _ \(_)    
//  | |__  | || |_| |_) |_ ___ 
//  |  __| |__   _|  _ <| / __|
//  | |____   | | | |_) | \__ \
//  |______|  |_| |____/|_|___/
//   an E4 Project
// </copyright>
// ================================================================================================

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NotesNET.Models;

namespace NotesNET.Data
{
    public class TodoDbContext : IdentityDbContext
    {
        public DbSet<TodoItem> Items { get; set; }

        public TodoDbContext(DbContextOptions options)
            : base(options)
        {
        }
    }
}