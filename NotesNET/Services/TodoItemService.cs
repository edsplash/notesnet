﻿using System.Collections.Generic;
using System.Linq;
using NotesNET.Data;
using NotesNET.Models;

namespace NotesNET.Services
{
    public class TodoItemService : ITodoItemService
    {
        private readonly TodoDbContext _context;

        public TodoItemService(TodoDbContext context)
        {
            _context = context;
        }

        public TodoItem Get(long id)
        {
            var item = _context.Items.FirstOrDefault(x => x.Id == id);
            if (item == null)
            {
                // TODO Welche exception schmeissen?
                throw new KeyNotFoundException();
            }

            return item;
        }

        public void AddItem(TodoItem item)
        {
            _context.Items.Add(item);
            _context.SaveChanges();
        }

        public void UpdateItem(TodoItem item)
        {
            _context.Items.Update(item);
            _context.SaveChanges();
        }

        public IEnumerable<TodoItem> SortOrder(OrderType orderBy, OrderDirection direction)
        {
            var allData = _context.Items;

            bool descending = direction == OrderDirection.Descending;

            if (orderBy == OrderType.FinishedDate)
            {
                return allData.OrderByWithDirection(t => t.FinishDate, descending);
            }
            else if (orderBy == OrderType.Importance)
            {
                return allData.OrderByWithDirection(t => t.Importance, descending);
            }
            else if (orderBy == OrderType.CreatedDate)
            {
                return allData.OrderByWithDirection(t => t.CreatedDate, descending);
            }

            return allData.ToList();
        }

        public IEnumerable<TodoItem> FilterFinished(bool filterFinished, IEnumerable<TodoItem> items)
        {
            if (filterFinished)
            {
                return items.Where(item => !item.Finished);
            }

            return items;
        }

    }
}