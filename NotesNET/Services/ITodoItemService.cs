﻿using System.Collections.Generic;
using NotesNET.Models;

namespace NotesNET.Services
{
    public interface ITodoItemService
    {
        TodoItem Get(long id);
        void AddItem(TodoItem item);
        void UpdateItem(TodoItem item);
        IEnumerable<TodoItem> SortOrder(OrderType orderBy, OrderDirection direction);
        IEnumerable<TodoItem> FilterFinished(bool filterFinished, IEnumerable<TodoItem> items);
    }
}