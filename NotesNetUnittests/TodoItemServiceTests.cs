﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NotesNET.Data;
using NotesNET.Models;
using NotesNET.Services;
using Xunit;

namespace NotesNetUnittests
{
    public class TodoItemServiceTests
    {
        [Fact]
        public void EditItem_WhenSuccessful_ReturnEditedItem()
        {
            var dbContext = GetDatabaseContext();
            var editableItem = GetInitializedItem();
            dbContext.Items.AddRange(editableItem);
            dbContext.SaveChanges();
            var service = new TodoItemService(dbContext);

            // Act
            editableItem.Importance = 1;
            service.UpdateItem(editableItem);

            // Assert
            Assert.Equal(editableItem, dbContext.Items.FirstOrDefault());
        }

        private TodoDbContext GetDatabaseContext()
        {
            var dbOptions = new DbContextOptionsBuilder();
            dbOptions.UseInMemoryDatabase("TodoTestDb");
            var dbContext = new TodoDbContext(dbOptions.Options);

            return dbContext;
        }

        private TodoItem GetInitializedItem()
        {
            return new TodoItem()
            {
                CreatedDate = DateTime.Now,
                FinishDate = DateTime.Now.AddDays(1),
                Finished = false,
                Importance = 5,
                Title = "DummyTitle",
                Text = "Dummy Text"
            };
        }
    }
}