using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NotesNET.Controllers;
using NotesNET.Models;
using NotesNET.Services;
using Xunit;

namespace NotesNetUnittests
{
    public class TodoControllerTests
    {

        [Fact]
        public void GetTodoItem_WhenItemNotExists_Returns404()
        {
            // Arrange
            var service = new Mock<ITodoItemService>();
            service.Setup(s => s.Get(It.IsAny<long>())).Returns<long>(null);
            var controller = new TodoController(service.Object, null);

            // Act
            var result = controller.Edit(1) as StatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public void GetTodoItem_WhenItemExists_ReturnsValidTodoItem()
        {
            // Arrange
            var itemService = new Mock<ITodoItemService>();
            itemService.Setup(s => s.Get(It.IsAny<long>())).Returns<long>(i => new TodoItem());
            var controller = new TodoController(itemService.Object, null);

            // Act
            var result = controller.Edit(1);

            // Assert
            Assert.NotNull(result);
        }
    }
}
